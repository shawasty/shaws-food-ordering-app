from django import forms



class CheckoutForm(forms.Form):
    email = forms.EmailField(max_length=25)
    first_name = forms.CharField(max_length=25)
    last_name = forms.CharField(max_length=25)
    address = forms.CharField(max_length=256)
    delivery_type = forms.CharField(max_length=25)
    card_name = forms.CharField(max_length=50)
    card_number = forms.CharField(max_length=25)
    cvv_number = forms.CharField(max_length=25)
    expiration_date = forms.DateField()