
#list of items
      #qty
      #pk
#number_items

#
#add item
#remove item
#clear


class ShoppingCart(object):
    def __init__(self, session):
        self.items=[]
        self.session = session
        if 'cart_items' in session:
            self.items=session['cart_items']

    def __str__(self):
        return "Cart with items sa,sa,sa,sas TOTAL: $77777"

    def add_item(self, item_id:int, quantity:int, name:str , price:int ):
        sub_total = float(price) * float(quantity)
        if self.check_if_item_in_cart(item_id):
            for i in self.items:
                if i["item_id"] == item_id:
                    i['qty'] = quantity
                    i['sub_total'] = sub_total
                    break
        else:
            item = {'item_id': item_id, 'qty': quantity , 'name': name , 'price':price ,'sub_total':sub_total} 
        

            self.items.append(item)
        self.session['cart_items'] = self.items
    
    def check_if_item_in_cart(self, item_id):
        for i in self.items:
            if i["item_id"] == item_id:
                return True
        return False

    def remove_item(self, item_id:int):
        item=None
        for i in self.items:
            if i['item_id'] == item_id:
                item=i
                break
        if item:
            self.items.remove(item)

        self.session['cart_items'] = self.items

    def clear_items(self):
        self.items.clear()
        self.session['cart_items'] = self.items

    def sum_total(self):
        total_list = []
        for i in self.items:
            total_list.append(float(i['price']) * float(i['qty']))
        return sum(total_list)