from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login , logout
from django.contrib.auth.forms import UserCreationForm ,AuthenticationForm
from django.contrib.auth.models import User

from .shopping_cart import ShoppingCart
from admin_m import forms as admin_forms
from admin_m.models import Meals, Order, OrderItem
from . import forms




def index(request):
    return render (request, template_name='customers/index.html')


def signup(request):
    if request.user.is_authenticated:
        return redirect('customers:menu_list')
    if request.method == 'POST':
        # form = UserCreationForm(request.POST)
        
        form = admin_forms.SignupForm(request.POST)
        if form.is_valid():
            
            email = form.cleaned_data.get('email')
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            password = form.cleaned_data.get('password')

            #cerate user
            user = User.objects.create_user(username=email, 
                                            email=email,
                                            password=password, 
                                            first_name=first_name, 
                                            last_name=last_name)

            
            login(request, user)
            return redirect('customer:menu_list')
        else:
            return render(request, 'customers/signup.html', {'form': form})
    else:
        form = admin_forms.SignupForm()
        return render(request, 'customers/signup.html', {'form': form})

def signin(request):
    if request.user.is_authenticated:
        return render(request, 'customers/menulist.html')
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('customers:menu_list')
        else:
            form = AuthenticationForm(request.POST)
            return render(request, 'customers/signin.html', {'form': form})
    else:
        form = AuthenticationForm()
        return render(request, 'customers/signin.html', {'form': form})


def menu(request):
    search=request.GET.get('search')
    if search:
        foods=Meals.objects.filter(name__startswith=search)
    
        

    else:
        foods = Meals.objects.all()


    return render(request,template_name='customers/menulist.html' , context={
                                                        'menu': foods})



def food_detail(request, pk):

    if request.method == 'POST':
        qty = request.POST.get('quantity')
        name = request.POST.get('name')
        price = request.POST.get('price')

        cart = ShoppingCart(session=request.session)
        cart.add_item(item_id=pk, quantity=qty, name= name , price= price )

        return redirect('customers:menu_list')
    else:
        food = Meals.objects.get(pk=pk)
        return render(request, template_name='customers/food_detail.html', context={
            'food': food
        })

def cart (request):
    purchased = request.session.get('cart_items')
    cart = ShoppingCart(session=request.session)
    total = cart.sum_total()
    return render (request, template_name='customers/cart_list.html', context={
        'cart' : purchased,
        'total': total
    })

def clear_cart (request):
    cart = ShoppingCart(session=request.session)
    cart.clear_items()
    return redirect('customers:menu_list')

def remove_item (request,pk):
    cart = ShoppingCart(session=request.session)
    cart.remove_item(pk)
    return redirect('customers:cart_list')


def checkout(request):
    form = forms.CheckoutForm()
    cart = request.session.get("cart_items")
    if request.method == 'POST':
        form = forms.CheckoutForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            order_items = [{
                "meal_id": item["item_id"],
                "quantity": item["qty"],
                "subtotal": item["sub_total"]
            } for item in cart]
            cart_object = ShoppingCart(session=request.session)
            total = cart_object.sum_total()

            order = Order.objects.create(
                email = data.get('email'),
                first_name = data.get('first_name'),
                last_name = data.get('last_name'),
                address = data.get('address'),
                delivery_type = data.get('delivery_type'),
                card_name = data.get('card_name'),
                card_number = data.get('card_number'),
                cvv_number = data.get('cvv_number'),
                expiration_date = data.get('expiration_date'),
                total = total
                )
            
            for item in order_items:
                order_items_created = OrderItem.objects.create(
                    meal_id=item.get('meal_id'),
                    subtotal=item.get('subtotal'),
                    quantity=item.get('quantity')
                )
                order.order_item.add(order_items_created) #i am addding the reference of OrderItem to the order model
            
            order.save()
            print(order_items)
            cart_object.clear_items()
            return redirect("customers:receipt")
    return render(request, template_name='customers/checkout.html', context={'form':form})


def receipt(request):
    return render(request, template_name='customers/receipt_page.html')


def track_order(request):
    context = {}
    if request.GET.get('tracking_number'):
        tracking_number = request.GET.get('tracking_number')
        order = Order.objects.filter(id=tracking_number).first()
        context['order'] = order
        context['tracking_number'] = tracking_number
        print(order.__dict__)
    return render(request, template_name='customers/track_order.html', context=context)
    