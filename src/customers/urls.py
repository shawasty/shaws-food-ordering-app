from django.contrib import admin
from django.urls import path
from customers import views
from django.contrib.auth import login ,logout

app_name = 'customers'

urlpatterns = [
    path('index/',view=views.index,name='index_page'),
    path('signup/',view=views.signup,name='signup_page'),
    path('signin/',view=views.signin,name='login_page'),
    path('',view=views.menu,name='menu_list'),
    path('detail/<int:pk>', view=views.food_detail, name='food_detail'),
    path('cart/',view=views.cart,name='cart_list'),
    path('clear-cart',view=views.clear_cart,name='clear_items'),
    path('remove-item/<int:pk>',view=views.remove_item,name='remove_item'),
    path('checkout/',view=views.checkout,name='checkout'),
    path('receipt/',view=views.receipt,name='receipt'),
    path('track-order/',view=views.track_order,name='track_order')
]
