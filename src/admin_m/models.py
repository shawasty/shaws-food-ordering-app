from django.db import models

# Create your models here.

class Meals(models.Model):
    name= models.CharField(max_length=50)
    category = models.CharField(max_length=30)
    image   = models.ImageField(upload_to='images', null= True)
    calories= models.DecimalField(max_digits=20,decimal_places=2)
    price = models.DecimalField(max_digits=20, decimal_places=2)

    def __str__(self):
        return f"{self.name} PRICE: {self.price}"


class OrderItem(models.Model):
    meal = models.ForeignKey(to=Meals, on_delete=models.CASCADE)
    subtotal = models.DecimalField(max_digits=20, decimal_places=2, default=0.00)
    quantity = models.IntegerField()


class Order(models.Model):
    ORDER_STATUS_NEW = "new"
    ORDER_STATUS_PROCESSING ="processing"
    ORDER_STATUS_READY = "ready"
    ORDER_STATUS_IN_TRANSIT = "transit"
    ORDER_STATUS_DELIVERED = "delivered"
    ORDER_STATUS_DECLINED ="declined"
    ORDER_STATUS_CANCELLED = "cancelled"

    ORDER_STATUS = (
    (ORDER_STATUS_NEW,"New"),
    (ORDER_STATUS_PROCESSING, "Processing"),
    (ORDER_STATUS_READY,"Ready"),
    (ORDER_STATUS_IN_TRANSIT, "On The Way"),
    (ORDER_STATUS_DELIVERED, "Delivered"),
    (ORDER_STATUS_DECLINED, "Declined"),
    (ORDER_STATUS_CANCELLED, "Cancelled"),
    )

    email = models.EmailField(max_length=25)
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25)
    address = models.CharField(max_length=256, )
    delivery_type = models.CharField(max_length=25)
    card_name = models.CharField(max_length=50)
    card_number = models.CharField(max_length=25)
    cvv_number = models.CharField(max_length=25)
    expiration_date = models.DateField()
    order_item = models.ManyToManyField(to=OrderItem)
    total = models.DecimalField(max_digits=20, decimal_places=2, default=0.00)
    status = models.CharField(max_length=25, choices=ORDER_STATUS, default=ORDER_STATUS_NEW)


    def __str__(self):
        return f"{self.first_name} {self.last_name}"
    