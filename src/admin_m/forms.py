
from django import forms


#email
#first name
#last name
#password
#confirm password


class SignupForm(forms.Form):
    email = forms.EmailField(max_length=25)
    first_name = forms.CharField(max_length=25)
    last_name = forms.CharField(max_length=25)
    password = forms.CharField(max_length=15, min_length=8)
    confirm_password = forms.CharField(max_length=15, min_length=8)



# class MealsForm(form.Form):
#     name= models.CharField(max_length=50)
#     category = forms.CharField(max_length=30)
#     image   = forms.ImageField(upload_to='static/images', null= True)
#     calories= forms.DecimalField(max_digits=20,decimal_places=2)
#     price = forms.DecimalField(max_digits=20, decimal_places=2)
    