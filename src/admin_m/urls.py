from django.contrib import admin
from django.urls import path
from admin_m import views
from django.contrib.auth import login ,logout

urlpatterns = [
    path('',view=views.menu,name='customers:menu_list'),
    path('signin/',view=views.signin,name='login_page'),
    path('signout/',view=views.signout,name='logout_page'),
    path('signup/',view=views.signup,name='signup_page'),
    path('welcome/',view=views.welcome,name='welcome_page'),
    path('menu/',view=views.menu,name='menu_list'),
    path('createmenu/',view=views.createmenu,name='createmenu_page'),
    path('delete/<int:pk>',view=views.delete,name='delete_page'),
    path('edit/<int:pk>',view=views.edit,name='edit_page'),
    path('orders',view=views.orders,name='orders'),
    path('orders/<int:pk>',view=views.order_detail,name='order_detail'),
    path('orders/<int:pk>/status',view=views.order_status_update,name='update_order_status'),
    
    

]
