from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login , logout
from django.contrib.auth.forms import UserCreationForm ,AuthenticationForm
from django.contrib.auth.models import User


from . import forms
from .models import Meals, Order




def index(request):
    return render (request, template_name='customers/menulist.html')

def welcome(request):
    return render (request, template_name='welcome.html')

def signup(request):
    if request.user.is_authenticated:
        return redirect('welcome_page')
    if request.method == 'POST':
        # form = UserCreationForm(request.POST)
        
        form = forms.SignupForm(request.POST)
        if form.is_valid():
            
            email = form.cleaned_data.get('email')
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            password = form.cleaned_data.get('password')

            #cerate user
            user = User.objects.create_user(username=email, 
                                            email=email,
                                            password=password, 
                                            first_name=first_name, 
                                            last_name=last_name)

            
            login(request, user)
            return redirect('welcome_page')
        else:
            return render(request, 'signup2.html', {'form': form})
    else:
        form = forms.SignupForm()
        return render(request, 'signup2.html', {'form': form})

def signin(request):
    if request.user.is_authenticated:
        return render(request, 'welcome.html')
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('welcome_page')
        else:
            form = AuthenticationForm(request.POST)
            return render(request, 'signin.html', {'form': form})
    else:
        form = AuthenticationForm()
        return render(request, 'signin.html', {'form': form})

def signout(request):
    logout(request)
    return redirect('login_page')


def createmenu(request):
    
    if request.method == 'POST':
        name = request.POST.get('name')
        category = request.POST.get('category')
        image= request.FILES.get('image')
        calories = request.POST.get('calories')
        price= request.POST.get('price')

        food= Meals(name=name,
                    category=category,
                    image=image,
                    calories=calories,
                    price=price)

        food.save()
        return redirect('menu_list')
    return  render(request,template_name='create_menu.html')

def menu(request):
    search=request.GET.get('search')
    if search:
        foods=Meals.objects.filter(name__startswith=search)
    
        

    else:
        foods = Meals.objects.all()


    return render(request,template_name='menulist.html' , context={
                                                        'menu': foods})


def delete(request, pk):

    
    food = Meals.objects.get(pk=pk)
    food.delete()

    return redirect('menu_list')
        

def edit(request, pk):
    # throw an exception
    try:
        food = Meals.objects.get(pk=pk)
        if request.method == 'POST':
            name = request.POST.get('name')
            category = request.POST.get('category')
            image= request.FILES.get('image')
            calories = request.POST.get('calories')
            price= request.POST.get('price')

            food.name=name
            food.category=category
            food.image=image
            food.calories=calories
            food.price=price
            food.save()
            return redirect('menu_list')

    except Meals.DoesNotExist:
        food = None
    
     

    return render(request,template_name='editmenu.html', context={'food': food})

def orders(request):
    orders = Order.objects.all()
    return render(request,template_name='orders.html', context={'orders': orders})


def order_detail(request,pk):
    order = Order.objects.filter(id=pk).first()
    return render(request,template_name='order_detail.html', context={'order': order})

def order_status_update(request, pk):
    status = request.GET.get('status')
    order = Order.objects.filter(id=pk).first()
    order.status = status
    order.save()
    return redirect('order_detail', pk=pk)
    # return render(request,template_name='order_detail.html', context={'message': 'Order Status has been updated successfully'})
        




